# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

	# setting private ip-address, please change this if running multiple servers for a unique IP
	config.vm.network "private_network", ip: "192.168.50.50"

	# we want to use easy host names and require the Vagrant plugin hostsupdater to be installed.
	unless Vagrant.has_plugin?("HostsUpdater")
  	raise 'Plugin HostsUpdater is missing, please run: vagrant plugin install vagrant-hostsupdater'
	end

	# we want to use VirtualBox Guest Additions and require the plugin installed.
	unless Vagrant.has_plugin?("vagrant-vbguest")
  	raise 'Plugin vbguest is missing, please run: vagrant plugin install vagrant-vbguest'
	end

	# setting host name and update the guest addition
  config.hostsupdater.aliases = [ "wpstarter.dev" ]
	config.vbguest.auto_update = false

	# set the box and hostname
  config.vm.box = "precise64"
  config.vm.hostname = "wpstarter"
  config.ssh.forward_agent = true

	config.vm.provider "virtualbox" do |v|
		v.name = "wpstarter"
		v.customize ["modifyvm", :id, "--cpuexecutioncap", "50"]
		v.memory = 1024
		v.cpus = 2
		v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
		v.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
		v.customize ["modifyvm", :id, "--natnet1", "192.168/16"]
	end

	# running a small set of update to the machine before starting Ansible
	config.vm.provision "shell", path: "provision.sh"
	config.vm.provision :shell, inline: "if [ ! $(grep single-request-reopen /etc/resolv.conf) ]; then echo 'options single-request-reopen' >> /etc/resolv.conf && service networking restart; fi"

	# this will activate ansible setup inside the local guest machine
	config.vm.provision "shell", inline: "chmod -x /vagrant/ansible/vagrant"
	config.vm.provision "shell", inline: "chmod -x /vagrant/ansible/localhost"
	config.vm.provision "shell", inline: "ansible-playbook -i /vagrant/ansible/localhost -v /vagrant/ansible/site.yml"

	# if you have ansible installed in the host, comment the above statement and uncomment the block below
	# config.vm.provision :ansible do |ansible|
	#	   ansible.playbook = "ansible/site.yml"
	#	   ansible.inventory_path = "ansible/vagrant"
	# end

	# Synced folder to gain local web developemnt
	config.vm.synced_folder "www/", "/var/www/wpstarter", owner: "www-data", group: "www-data", :create=>true

	# if you want to use NFS for faster sync disable the synced config above and enable this one:
	# config.vm.synced_folder "www/", "/var/www/wpstarter", create: "true", type: "nfs", :mount_options => ['nolock,vers=3,udp']

	# if you enjoy the rsync-option, please use this row and use the vagrant rsync-auto!
	# config.vm.synced_folder "www/", "/var/www/wpstarter", owner: "www-data", group: "www-data", :create=>true, type: "rsync", rsync__exclude: ".git/", rsync__args: ["--verbose", "--archive", "-z"]

end